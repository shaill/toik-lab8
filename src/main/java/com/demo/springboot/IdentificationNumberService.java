package com.demo.springboot;

import org.springframework.stereotype.Service;

@Service
public class IdentificationNumberService {

    public boolean validate(String pesel){
        if(pesel.length()!=11) {
            return false;
        }
        byte peselArray[] = new byte[11];
        for (int i = 0; i < pesel.length(); i++){
            peselArray[i] = Byte.parseByte(pesel.substring(i, i+1));
        }
        int sum = 9 * peselArray[0] +
                7 * peselArray[1] +
                3 * peselArray[2] +
                1 * peselArray[3] +
                9 * peselArray[4] +
                7 * peselArray[5] +
                3 * peselArray[6] +
                1 * peselArray[7] +
                9 * peselArray[8] +
                7 * peselArray[9];
        sum %= 10;

        return sum==peselArray[10];

    }
}
